datalife_purchase_shipment_method
=================================

The purchase_shipment_method module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-purchase_shipment_method/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-purchase_shipment_method)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
