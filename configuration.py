# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool


class Configuration(metaclass=PoolMeta):
    __name__ = 'purchase.configuration'

    purchase_shipment_method = fields.MultiValue(fields.Selection([
            ('manual', 'Manual'),
            ('order', 'Based on order')
            ], 'Purchase shipment method', required=True))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'purchase_shipment_method':
            return pool.get('purchase.configuration.purchase_method')
        return super(Configuration, cls).multivalue_model(field)

    @classmethod
    def default_purchase_shipment_method(cls, **pattern):
        return cls.multivalue_model(
            'purchase_shipment_method').default_purchase_shipment_method()


class ConfigurationPurchaseMethod(metaclass=PoolMeta):
    __name__ = 'purchase.configuration.purchase_method'

    purchase_shipment_method = fields.Selection([
            ('manual', 'Manual'),
            ('order', 'Based on order')
            ], 'Purchase shipment method', required=True)

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)
        if exist:
            exist &= table_h.column_exist('purchase_shipment_method')

        super(ConfigurationPurchaseMethod, cls).__register__(module_name)

        if not exist:
            # Re-migration
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('purchase_shipment_method')
        value_names.append('purchase_shipment_method')
        super(ConfigurationPurchaseMethod, cls)._migrate_property(
            field_names, value_names, fields)

    @classmethod
    def default_purchase_shipment_method(cls):
        return 'order'
