# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    shipment_method = fields.Function(fields.Selection([
            ('manual', 'Manual'),
            ('order', 'Based on order')
            ], 'Shipment method'), 'get_shipment_method')

    def create_move(self, move_type):
        pool = Pool()
        Shipment = pool.get('stock.shipment.in')

        moves = super(Purchase, self).create_move(move_type)
        if self._must_create_shipment(move_type, moves):
            shipment = self._get_shipment_purchase(Shipment)
            shipment.moves = moves
            shipment.save()
        return moves

    def _must_create_shipment(self, move_type, moves):
        return moves and move_type == 'in' and self.shipment_method == 'order'

    def get_shipment_method(self, name=None):
        pool = Pool()
        Configuration = pool.get('purchase.configuration')
        return Configuration(1).purchase_shipment_method

    def _get_shipment_purchase(self, Shipment):
        stockshipment = Shipment(
            supplier=self.party,
            company=self.company,
            warehouse=self.warehouse,
            planned_date=self.delivery_date or self.purchase_date,
            effective_date=self.delivery_date or self.purchase_date
            )
        stockshipment.on_change_supplier()
        return stockshipment
